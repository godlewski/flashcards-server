# coding=utf-8
from datetime import datetime
import string
from django.http import HttpResponse
from api import utils
from api.models import Category, Language, User

__author__ = 'lgi'

def add_12_cats(request):
    cats = [("flagi państw europejskich", "pl", ["geografia", "europa", "flagi"]),
            ("stolice państw europejskich", "pl", ["geografia", "stolice", "europa", "państwa"]),
            ("phrasal verbs", "pl", ["angielski", "phrasals"]),
            ("zdania po angielsku cz. 1", "pl", ["angielski", "zdania", "nauka"]),
            ("zdania po angielsku cz. 2", "pl", ["angielski", "zdania", "nauka"]),
            ("zdania po angielsku cz. 3", "pl", ["angielski", "zdania", "nauka"]),
            ("historia ogólna", "pl", ["historia", "daty", "wydarzenia"]),
            ("daty wydarzeń z XX wieku", "pl", ["historia", "daty", "wydarzenia"]),
            ("spain words", "eng", ["spain", "learn", "language"]),
            ("french words", "eng", ["french", "learn", "language"]),
            ("german words", "eng", ["german", "learn", "language"]),
            ("słówka po niemiecku", "pl", ["języki", "słówka", "niemiecki"])]

    for name, lang, keywords in cats:
        c = Category()
        c.name = name
        c.count = 0
        c.language = Language.objects.get_or_create(code=lang, defaults={'name': lang})[0]
        c.owner = User.objects.get_or_create(email="lgi@o2.pl", name="lgi", password=utils.hash_text("lgi"))[0]
        c.modification_date = datetime.now()
        c.keywords = string.join(keywords, " ")
        c.votes = 0
        c.downloads = 0
        c.mean_vote = 0
        c.save()

    return HttpResponse("OK")
