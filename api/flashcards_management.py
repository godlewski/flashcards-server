from django.db.models import Q, Sum
import operator
from api.models import Session, Category, Flashcard, User, Language, Vote
from response import Response
import datetime
import error_code

def create_category(data):
    # sprawdzenie sesji
    if not check_session(data):
        return Response(False, error_code.SESSION_EXPIRED)

    # sprawdzenie, czy kategoria juz istnieje
    if Category.objects.filter(name=data.category['name']).exists():
        return Response(False, error_code.CATEGORY_ALREADY_EXISTS)

    # dodanie kategorii
    user = User.objects.get(name=data.login)
    category = Category(
        name=data.category['name'],
        owner=user,
        count=0,
        modification_date=datetime.datetime.now())
    if 'language' in data.category:
        lang_id = data.category['language']
        category.language = Language.objects.get_or_create(code=lang_id, defaults={'name': lang_id})[0]
    category.keywords = data.keywords
    category.votes = 0
    category.downloads = 0
    category.mean_vote = 0
    category.save()

    # dodanie fiszek
    for f in data.flashcards:
        Flashcard(first_page=f['first_page'],
            second_page=f['second_page'],
            first_page_type=f['first_page_type'],
            second_page_type=f['second_page_type'],
            category_id=category.id).save()
    category.count = len(data.flashcards)
    category.save()

    return Response(True)


def check_session(data):
    try:
        session = Session.objects.get(user__name=data.login, key=data.session_id)
    except Session.DoesNotExist:
        return False

    if session.finish_time < datetime.datetime.now():
        return False

    session.finish_time = datetime.datetime.now() + datetime.timedelta(hours=1)
    session.save()

    return True


def retrieve_categories(data):
    if not check_session(data):
        return Response(False, error_code.SESSION_EXPIRED)

    if data.phrase is not None:
        constraint_list = [Q(keywords__contains=x) for x in data.phrase.split()]
        constraint = reduce(operator.or_, constraint_list + [Q(name__contains=data.phrase)])
        items = Category.objects.select_related().filter(constraint).order_by('id')
    else:
        items = Category.objects.select_related().all().order_by('id')

    items = items[data.size * data.position:data.size * (data.position + 1)]
    return Response(True, [category_to_object(x) for x in items])


def download_category(data):
    if not check_session(data):
        return Response(False, error_code.SESSION_EXPIRED)
    try:
        category = Category.objects.get(id=data.id)
    except Session.DoesNotExist:
        return Response(False, error_code.CATEGORY_NOT_EXISTS)
    result = category_to_object(category,
        [flashcard_to_object(f) for f in Flashcard.objects.filter(category=category)[:]])
    category.downloads += 1
    category.save()
    return Response(True, result)


def vote_category(data):
    if not check_session(data):
        return Response(False, error_code.SESSION_EXPIRED)

    try:
        category = Category.objects.get(id=data.id)
    except Session.DoesNotExist:
        return Response(False, error_code.CATEGORY_NOT_EXISTS)
    user = User.objects.get(name=data.login)

    vote, created = Vote.objects.get_or_create(user__name=data.login,
        defaults={'category': category, 'vote': 0, 'user': user, 'comment': ''})
    vote.vote = data.vote
    vote.comment = data.comment
    vote.save()
    if created: category.votes += 1
    sum_votes = Vote.objects.filter(category=category).aggregate(Sum('vote'))['vote__sum']
    category.mean_vote = 5 * sum_votes / category.votes
    category.save()
    return Response(True)


def category_to_object(category, flashcards=None):
    obj = {'id': category.id,
           'name': category.name,
           'count': category.count,
           'language': category.language.code,
           'votes': category.votes,
           'downloads': category.downloads,
           'mean_vote': category.mean_vote}
    if flashcards is not None:
        obj['flashcards'] = flashcards
    return obj


def flashcard_to_object(flashcard):
    return {'first_page': flashcard.first_page,
            'first_page_type': flashcard.first_page_type,
            'second_page': flashcard.second_page,
            'second_page_type': flashcard.second_page_type}