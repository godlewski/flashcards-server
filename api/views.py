from api import user_management, flashcards_management, files_management
from api.json_parse import LoginRequest, RegisterRequest, AddCategoryRequest,\
    RetrieveCategoriesRequest, DownloadCategoryRequest, VoteCategoryRequest
from django.http import HttpResponse
import error_code
import json

def register(request):
    return execute_action(request, RegisterRequest, user_management.register)


def login(request):
    return execute_action(request, LoginRequest, user_management.login)


def add_category(request):
    return execute_action(request, AddCategoryRequest, flashcards_management.create_category)


def retrieve_categories(request):
    return execute_action(request, RetrieveCategoriesRequest, flashcards_management.retrieve_categories)


def download_category(request):
    return execute_action(request, DownloadCategoryRequest, flashcards_management.download_category)


def vote_category(request):
    return execute_action(request, VoteCategoryRequest, flashcards_management.vote_category)


def upload_file(request):
    result = files_management.upload_file(request)
    if result.is_ok():
        return response_ok(result.data)
    else:
        return response_error(result.data)


def execute_action(request, input_type, action_function):
    json_string = retrieve_json_string(request)
    if json_string is not None:
        parsed_data = input_type(json_string)
        if parsed_data.is_valid():
            result = action_function(parsed_data)
            if result.is_ok():
                return response_ok(result.data)
            else:
                return response_error(result.data)
    return response_error(error_code.WRONG_DATA)


def retrieve_json_string(request):
    if request.method == 'POST':
        return 'json' in request.POST and request.POST['json'] or None


def response_ok(params=None):
    data = {"result": "OK"}
    if params is not None:
        data["data"] = params
    result = json.dumps(data)
    return HttpResponse(result)


def response_error(error_code):
    result = json.dumps({"result": "ERROR", "error_code": error_code})
    return HttpResponse(result)
