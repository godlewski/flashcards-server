from api import utils
from api.models import User, Session
from django.db.models import Q
from response import Response
import error_code
import uuid
import datetime

def register(data):
    if User.objects.filter(
        Q(name__contains=data.login) | Q(email__contains=data.email)).count() > 0:
        return Response(False, error_code.USER_ALREADY_EXISTS)

    User(
        email=data.email,
        name=data.login,
        password=utils.hash_text(data.password)).save()
    return Response(True)


def login(data):
    login = data.login
    password = utils.hash_text(data.password)
    user = None

    try:
        user = User.objects.get(name=login, password=password)
    except User.DoesNotExist:
        return Response(False, error_code.INVALID_LOGIN_OR_PASSWORD)

    #usuwanie poprzednich sesji
    Session.objects.filter(user__name=login).delete()

    session_id = utils.hash_text(uuid.uuid4().__str__())
    ft = datetime.datetime.now() + datetime.timedelta(hours=2)
    Session(user=user, key=session_id, finish_time=ft).save()

    return Response(True, session_id)
