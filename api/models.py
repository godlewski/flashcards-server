from django.db import models

class User(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=30)
    password = models.CharField(max_length=32)


class Device(models.Model):
    owner = models.ForeignKey(User)
    name = models.CharField(max_length=50)


class Session(models.Model):
    user = models.ForeignKey(User)
    key = models.CharField(max_length=32)
    finish_time = models.DateTimeField()


class Language(models.Model):
    code = models.CharField(max_length=3, primary_key=True)
    name = models.CharField(max_length=20)


class Category(models.Model):
    name = models.CharField(max_length=25)
    keywords = models.CharField(max_length=150)
    language = models.ForeignKey(Language, null=True, blank=True)
    count = models.IntegerField()
    downloads = models.IntegerField()
    votes = models.IntegerField()
    mean_vote = models.FloatField()
    owner = models.ForeignKey(User)
    modification_date = models.DateTimeField()


class Flashcard(models.Model):
    first_page = models.CharField(max_length=250)
    second_page = models.CharField(max_length=250)
    first_page_type = models.IntegerField()
    second_page_type = models.IntegerField()
    category = models.ForeignKey(Category)


class Vote(models.Model):
    category = models.ForeignKey(Category)
    user = models.ForeignKey(User)
    vote = models.IntegerField()
    comment = models.CharField(max_length=250)
