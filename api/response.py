'''
Created on 11-11-2012

@author: lgi
'''

class Response:
    def __init__(self, ok, data=None):
        self.ok = ok
        self.data = data

    def is_ok(self):
        return self.ok

    def get_data(self):
        return self.data
