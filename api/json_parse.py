'''
Created on 09-11-2012

@author: lgi
'''

import json
import operator

class Request():
    def __init__(self, json_string):
        self.json_object = json.loads(json_string)

    def _fetch(self, key):
        if key in self.json_object:
            return self.json_object[key]

    def _contains_fields(self, *fields):
        return reduce(operator.and_, [field in self.json_object for field in fields])


class LoginRequest(Request):
    def __init__(self, json_string):
        Request.__init__(self, json_string)
        self.login = self._fetch('login')
        self.password = self._fetch('password')

    def is_valid(self):
        return self.login is not None and self.password is not None


class RegisterRequest(Request):
    def __init__(self, json_string):
        Request.__init__(self, json_string)
        self.login = self._fetch('login')
        self.password = self._fetch('password')
        self.email = self._fetch('email')

    def is_valid(self):
        return self.login is not None and self.password is not None and self.email is not None


class BaseActionRequest(Request):
    def __init__(self, json_string):
        Request.__init__(self, json_string)
        self.login = self._fetch('login')
        self.session_id = self._fetch('session_id')

    def is_valid(self):
        return self.login is not None and self.session_id is not None


class AddCategoryRequest(BaseActionRequest):
    def __init__(self, json_string):
        BaseActionRequest.__init__(self, json_string)
        self.category = self._fetch('category')
        self.flashcards = self._fetch('flashcards')

    def is_valid(self):
        if not BaseActionRequest.is_valid(self) or self.category is None or self.flashcards is None:
            return False

        if len(self.flashcards) == 0:
            return False

        if not 'keywords' in self.category:
            return False

        keywords_len = len(self.category['keywords'].split())
        if keywords_len == 0 or keywords_len > 5:
            return False

        for f in self.flashcards:
            if not 'first_page' in f\
               or not 'second_page' in f\
               or not 'first_page_type' in f\
            or not 'second_page_type' in f:
                return False

        return True


class RetrieveCategoriesRequest(BaseActionRequest):
    def __init__(self, json_string):
        BaseActionRequest.__init__(self, json_string)
        self.size = self._fetch('size')
        self.position = self._fetch('position')
        self.phrase = self._fetch('phrase')

    def is_valid(self):
        return BaseActionRequest.is_valid(self) and self._contains_fields('size', 'position')


class DownloadCategoryRequest(BaseActionRequest):
    def __init__(self, json_string):
        BaseActionRequest.__init__(self, json_string)
        self.id = self._fetch('id')

    def is_valid(self):
        return BaseActionRequest.is_valid(self) and self._contains_fields('id')


class VoteCategoryRequest(BaseActionRequest):
    def __init__(self, json_string):
        BaseActionRequest.__init__(self, json_string)
        self.id = self._fetch('id')
        self.vote = self._fetch('vote')
        self.comment = self._fetch('comment')

    def is_valid(self):
        return BaseActionRequest.is_valid(self) and self._contains_fields('id', 'vote', 'comment')
