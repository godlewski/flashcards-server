'''
Created on 18-11-2012

@author: lgi
'''

from api import error_code, flashcards_management
from api.json_parse import BaseActionRequest
from api.response import Response
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
import os
import random
import string

def upload_file(request):
    if request.method == 'POST' and 'json' in request.POST:
        json = BaseActionRequest(request.POST['json'])
        if not flashcards_management.check_session(json):
            return Response(False, error_code.INVALID_LOGIN_OR_PASSWORD)
        f = request.FILES['file_field']
        ext = os.path.splitext(f.name)[1]
        name = 'tmp/' + ''.join(random.choice(string.ascii_uppercase + string.digits) for json in range(10)) + ext
        path = default_storage.save(name, ContentFile(f.read()))
        return Response(True, default_storage.url(path))
    else:
        return Response(False, error_code.WRONG_DATA)
