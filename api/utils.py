import hashlib

__author__ = 'lgi'

def hash_text(text):
    md5 = hashlib.md5()
    md5.update(text)
    return md5.hexdigest()