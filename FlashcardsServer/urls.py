from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^api/register/', 'api.views.register'),
    url(r'^api/login/', 'api.views.login'),
    url(r'^api/add_category/', 'api.views.add_category'),
    url(r'^api/upload_file/', 'api.views.upload_file'),
    url(r'^api/retrieve_categories/', 'api.views.retrieve_categories'),
    url(r'^api/download_category/', 'api.views.download_category'),
    url(r'^api/vote_category/', 'api.views.vote_category'),
    url(r'^api/test/add_categories/', 'api.test.add_12_cats'),
)
